---
title: "Numbers, points, and lines"
output: 
  pdf_document: 
    keep_tex: yes
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Words ran away

... so I'm re-laying out the notes from previous meeting...

### Hypothesis

Follow the low-energy pathway of dehydration for natrolite, scolecite, and mesolite.  Compare against previous investigation's results for the former two zeolites, and compare against experimental findings (XRD, TGA).

If we follow the low-energy pathways, do we match experiment?

What happens if we follow the high-energy pathways?

![Natrolite dehydration enthalpies, as calculated by GULP](figs/fig_diffs_nat.pdf)

![Scolecite dehydration enthalpies, as calculated by GULP](figs/fig_diffs_sco.pdf)

![Mesolite dehydration enthalpies, as calculated by GULP](figs/fig_diffs_mes.pdf)

![Natrolite, scolecite, and mesolite dehydration enthalpies plotted on the same axis, just for the sake of it](figs/fig_diffs.pdf)

### Mesolite dehydration ordering

The calcium ions have been shown experimentally to be partially dehydrated first; however, simulations so far have predicted the loss of water to come from the sodium ions first.  High-energy pathways still do not predict the experimentally-found ordering.  This is likely to be a result of the potentials used.

To get around this, we can either work with quantum codes (which is underway) or fit new potentials to better represent the strength of binding between calcium, sodium, and water.

### Potassium-exchanged natrolite

The Next Step.
