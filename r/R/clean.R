# Clean up data

# Sort out column-naming faffs
nat_gulp <- geninfo.convert(nat_gulp)
sco_gulp <- geninfo.convert(sco_gulp)
mes_gulp <- geninfo.convert(mes_gulp)
lau_ambient <- geninfo.convert(lau_ambient)
leo_ambient <- geninfo.convert(leo_ambient)
nat_gulp_orig <- geninfo.convert(nat_gulp_orig)
sco_gulp_orig <- geninfo.convert(sco_gulp_orig)
nat_cp2k_vol <- cp2k.volheads(nat_cp2k_vol)
nat_cp2k_ene <- cp2k.eneheads(nat_cp2k_ene)

# Ensures all numeric fields are specified as numeric, just in case
nat_gulp$ene <- as.numeric(nat_gulp$ene)
# etc ...

# Sort out adding and shuffling of columns
# Adding...
nat_gulp <- column.add(nat_gulp)
sco_gulp <- column.add(sco_gulp)
mes_gulp <- column.add(mes_gulp)
lau_ambient <- column.add(lau_ambient)
leo_ambient <- column.add(leo_ambient)
nat_gulp_orig <- column.add(nat_gulp_orig)
sco_gulp_orig <- column.add(sco_gulp_orig)
# Shuffling...
nat_gulp <- column.shuffle(nat_gulp)
sco_gulp <- column.shuffle(sco_gulp)
mes_gulp <- column.shuffle(mes_gulp)
lau_ambient <- column.shuffle(lau_ambient)
leo_ambient <- column.shuffle(leo_ambient)
nat_gulp_orig <- column.shuffle(nat_gulp_orig)
sco_gulp_orig <- column.shuffle(sco_gulp_orig)
# Both at once
nat_cp2k_vol <- cp2k.vol.column.faff(nat_cp2k_vol)
nat_cp2k_ene <- cp2k.ene.column.faff(nat_cp2k_ene)

###################################################################################
# GULP
###################################################################################

# Calculate current delta-H values and add zero as first value
nat_gulp_ene_deltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff(nat_gulp$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))
#nat_gulp_ene_pcdeltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff((((nat_gulp$ene_kj - water_stuff[3,2])/nat_gulp[1,5]) * -100), lag = 1, differences = 1)))
sco_gulp_ene_deltas <- cbind(1:(nrow(sco_gulp)-1), (24-(0:(nrow(sco_gulp)-2))), (diff(sco_gulp$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))
mes_gulp_ene_deltas <- cbind(1:(nrow(mes_gulp)-1), (64-(0:(nrow(mes_gulp)-2))), (diff(mes_gulp$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))

# Tabulate first investigation's deltas
# NB: The original fully-hydrated one isn't included in the first investigation's data files for some reason, ditto for fully-dehydrated
nat_gulp_orig_deltas <- cbind(1:(nrow(nat_gulp_orig)-1), (16-(0:(nrow(nat_gulp_orig)-2))), (diff(nat_gulp_orig$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))
sco_gulp_orig_deltas <- cbind(1:(nrow(sco_gulp_orig)-1), (24-(0:(nrow(sco_gulp_orig)-2))), (diff(sco_gulp_orig$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))

# Averages for deltas (GULP)
nat_gulp_ene_deltas_avg <- mean(nat_gulp_ene_deltas[1:16,3])
sco_gulp_ene_deltas_avg <- mean(sco_gulp_ene_deltas[1:24,3])
sco_gulp_ene_deltas_stage1_avg <- mean(sco_gulp_ene_deltas[9:24,3])
sco_gulp_ene_deltas_stage2_avg <- mean(sco_gulp_ene_deltas[1:8,3])
mes_gulp_ene_deltas_avg <- mean(mes_gulp_ene_deltas[1:64,3])
mes_gulp_ene_deltas_stage1_avg <- mean(mes_gulp_ene_deltas[32:63,3])
mes_gulp_ene_deltas_stage2_avg <- mean(mes_gulp_ene_deltas[1:31,3])

# Volumes themselves for GULP stuffs
nat_gulp_vols <- cbind(1:(nrow(nat_gulp)), (16-(0:(nrow(nat_gulp)-1))), nat_gulp$vol)
nat_gulp_vols_fractional <- cbind(1:(nrow(nat_gulp)), (16-(0:(nrow(nat_gulp)-1))), nat_gulp$vol/nat_gulp[1,13])

# Volume changes for GULP stuffs
nat_gulp_vol_deltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff(nat_gulp$vol, lag = 1, differences = 1)))
sco_gulp_vol_deltas <- cbind(1:(nrow(sco_gulp)-1), (24-(0:(nrow(sco_gulp)-2))), (diff(sco_gulp$vol, lag = 1, differences = 1)))
mes_gulp_vol_deltas <- cbind(1:(nrow(mes_gulp)-1), (64-(0:(nrow(mes_gulp)-2))), (diff(mes_gulp$vol, lag = 1, differences = 1)))

# Cell parameter changes for GULP stuffs
nat_gulp_a <- cbind(1:(nrow(nat_gulp)), (16-(0:(nrow(nat_gulp)-1))), nat_gulp$a)
nat_gulp_b <- cbind(1:(nrow(nat_gulp)), (16-(0:(nrow(nat_gulp)-1))), nat_gulp$b)
nat_gulp_c <- cbind(1:(nrow(nat_gulp)), (16-(0:(nrow(nat_gulp)-1))), nat_gulp$c)

# Cell parameter changes for GULP stuffs
nat_gulp_abc_deltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff(nat_gulp$a, lag = 1, differences = 1)))
nat_gulp_abc_deltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff(nat_gulp$b, lag = 1, differences = 1)))
nat_gulp_abc_deltas <- cbind(1:(nrow(nat_gulp)-1), (16-(0:(nrow(nat_gulp)-2))), (diff(nat_gulp$c, lag = 1, differences = 1)))

sco_gulp_abc_deltas <- cbind(1:(nrow(sco_gulp)-1), (24-(0:(nrow(sco_gulp)-2))), (diff(sco_gulp$a, lag = 1, differences = 1)))
sco_gulp_abc_deltas <- cbind(1:(nrow(sco_gulp)-1), (24-(0:(nrow(sco_gulp)-2))), (diff(sco_gulp$b, lag = 1, differences = 1)))
sco_gulp_abc_deltas <- cbind(1:(nrow(sco_gulp)-1), (24-(0:(nrow(sco_gulp)-2))), (diff(sco_gulp$c, lag = 1, differences = 1)))

mes_gulp_abc_deltas <- cbind(1:(nrow(mes_gulp)-1), (64-(0:(nrow(mes_gulp)-2))), (diff(mes_gulp$a, lag = 1, differences = 1)))
mes_gulp_abc_deltas <- cbind(1:(nrow(mes_gulp)-1), (64-(0:(nrow(mes_gulp)-2))), (diff(mes_gulp$b, lag = 1, differences = 1)))
mes_gulp_abc_deltas <- cbind(1:(nrow(mes_gulp)-1), (64-(0:(nrow(mes_gulp)-2))), (diff(mes_gulp$c, lag = 1, differences = 1)))

###################################################################################
# CP2K
###################################################################################

# Energy changes for CP2K stuffs
nat_cp2k_ene_deltas <- cbind(1:(nrow(nat_cp2k_ene)-1), (15-(0:(nrow(nat_cp2k_ene)-2))), (diff(nat_cp2k_ene$ene_kj, lag = 1, differences = 1) - water_stuff[3,2]))
#nat_cp2k_ene_pcdeltas <- cbind(1:(nrow(nat_cp2k_ene)-1), (15-(0:(nrow(nat_cp2k_ene)-2))), (diff((((nat_cp2k_ene$ene_kj - water_stuff[3,2])/nat_cp2k_ene[1,5]) * -100), lag = 1, differences = 1)))

# Volumes themselves for CP2K stuffs
#nat_cp2k_vols <- cbind(1:(nrow(nat_cp2k_ene)), (16-(0:(nrow(nat_cp2k_ene)-1))), nat_cp2k_vol$vol)
nat_cp2k_vols <- cbind(1:(nrow(nat_cp2k_vol_test)), (16-(0:(nrow(nat_cp2k_vol_test)-1))), nat_cp2k_vol_test[3])
nat_cp2k_vols_fractional <- cbind(1:(nrow(nat_cp2k_vol_test)), (16-(0:(nrow(nat_cp2k_vol_test)-1))), nat_cp2k_vol_test[3]/nat_gulp[1,13])

# Volume changes for CP2K stuffs
nat_cp2k_vol_deltas <- cbind(1:(nrow(nat_cp2k_vol)-1), (15-(0:(nrow(nat_cp2k_vol)-2))), (diff(nat_cp2k_vol$vol, lag = 1, differences = 1)))

# Cell parameters themselves for CP2K stuffs
nat_cp2k_a <- cbind(1:(nrow(nat_cp2k_vol)), (16-(0:(nrow(nat_cp2k_vol)-1))), nat_cp2k_vol$a)
nat_cp2k_b <- cbind(1:(nrow(nat_cp2k_vol)), (16-(0:(nrow(nat_cp2k_vol)-1))), nat_cp2k_vol$b)
nat_cp2k_c <- cbind(1:(nrow(nat_cp2k_vol)), (16-(0:(nrow(nat_cp2k_vol)-1))), nat_cp2k_vol$c)

# Cell parameter changes for CP2K stuffs
nat_cp2k_abc_deltas <- cbind(1:(nrow(nat_cp2k_vol)-1), (15-(0:(nrow(nat_cp2k_vol)-2))), (diff(nat_cp2k_vol$a, lag = 1, differences = 1)))
nat_cp2k_abc_deltas <- cbind(1:(nrow(nat_cp2k_vol)-1), (15-(0:(nrow(nat_cp2k_vol)-2))), (diff(nat_cp2k_vol$b, lag = 1, differences = 1)))
nat_cp2k_abc_deltas <- cbind(1:(nrow(nat_cp2k_vol)-1), (15-(0:(nrow(nat_cp2k_vol)-2))), (diff(nat_cp2k_vol$c, lag = 1, differences = 1)))

###################################################################################
# TODO
###################################################################################
# - Tabulate scolecite stuffs
# - Tabulate mesolite stuffs
# - Output CSV files
#write.csv(nat_comparisons, file = "data/processed/nat.csv", row.names = FALSE, na = "")
